
import urllib2
import feedparser

from lxml import etree


class Source(object):

    def find(self):
        raise NotImplementedError("Should have implemented this")


class KijijiRSS(Source):

    def __init__(self, url, restriction=None):
        self.url = url
        self.restriction = restriction
        self.last_update = None

    def _parse_posting_page(self, page_html):

        parser = etree.HTMLParser()
        tree = etree.parse(page_html, parser)

        cost_path = "//table[@class ='ad-attributes']/tr[3]/td/div/span/strong"
        cost = tree.xpath(cost_path)[0].text

        address_path = "//table[@class = 'ad-attributes']/tr[4]/td"
        address = tree.xpath(address_path)[0].text

        bathrooms_path = "//table[@class = 'ad-attributes']/tr[6]/td"
        bathrooms = tree.xpath(bathrooms_path)[0].text

        user_info_path = "//div[@id = 'UserContent']/table"
        user_info = etree.tostring(tree.xpath(user_info_path)[0],
                                   method='text', encoding='utf-8')

        info_table_path = "//table[@class = 'ad-attributes']"
        info_table = etree.tostring(tree.xpath(info_table_path)[0],
                                    method='xml', encoding='utf-8',
                                    pretty_print=True)

        posting = {}
        #FIXME: this should be read as they are available,
        #not hardcoded

        posting['cost'] = cost
        posting['address'] = address
        posting['bathrooms'] = bathrooms

        posting['user_info'] = user_info
        posting['info_table'] = info_table

        return posting

    def find(self, ignore_date_update=False):

        d = feedparser.parse(self.url)

        postings = []
        new_last_update = self.last_update
        for entry in d.entries:

            published_date = entry['published_parsed']

            if not ignore_date_update:

                # validating that the posting is a new one
                if self.last_update is not None and \
                        published_date <= self.last_update:
                    continue

                if new_last_update is None or \
                        new_last_update < published_date:
                    new_last_update = published_date

            link = entry['link']

            page_html = urllib2.urlopen(link, " ")

            if page_html.geturl().find("adRemoved") != -1:
                continue

            posting = self._parse_posting_page(page_html)

            posting['link'] = link

            if self.restriction is None:
                postings.append(posting)
            else:
                if self.restriction.is_valid(posting):
                    postings.append(posting)

        self.last_update = new_last_update
        return postings
