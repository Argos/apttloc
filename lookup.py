from restrictions import RadiousRestriction
from geocoding import OSMGeocoding
from sources import KijijiRSS
from actions import SingleCSV, MultipleAction, QuequeAction

from ConfigParser import SafeConfigParser


import time

import multiprocessing as mltpro
import Queue

import logging
import logging.config

import msvcrt

logging.config.fileConfig('logging.conf')

# create logger
logger = logging.getLogger('lookup')

#load personal info
conf_parser = SafeConfigParser()
conf_parser.read('info.conf')

USER_EMAIL = conf_parser.get('user_info', 'email')


# Here we have two kinds of actions. the firat one, self.action, is excecuted
# immideatly as the postings are retreaved. The second one self.ascr_action is
# excecuted asynchrononicly, every self.ascr_sleeep_time minutes.
class LookUp(object):

    def __init__(self, source, action, waiting_time,
                 ascr_action=None, ascr_sleeep_time=None):
        self.source = source
        self.action = action
        #in minutes
        self.waiting_time = waiting_time

        self.ascr_action = ascr_action

        self.ascr_sleeep_time = ascr_sleeep_time

        self.thread = None
        if self.ascr_action is not None and (self.ascr_sleeep_time is None or \
                                             self.ascr_sleeep_time <= 0):
            raise Exception("if an asynchronous action is provided  you" + \
                            " must also provide am asynchronous waiting time ")

        if self.ascr_action is not None:
            self.q = mltpro.Queue()

    def run(self):

        if self.ascr_action is not None:
            newAction = MultipleAction([self.action])
            newAction.appendAction(QuequeAction(self.q))

            self.thread = mltpro.Process(target=_ascrActions,
                                         name='_ascrActions',
                                         args=(self.q, self.ascr_action,
                                               self.ascr_sleeep_time))
            self.thread.daemon = True
            self.thread.start()
            logger.info('asc deamon is running...')

        self._start()

    def _start(self):

        while True:

            logger.info('_start is excecuting....')
            new_postings = self.source.find()
            self.action.do(new_postings)

            # if waiting time is a negative number
            # then we run the query just one time
            if self.waiting_time < 0:
                break

            #seconds in a minute
            time.sleep(self.waiting_time * 60)

            if msvcrt.kbhit():
                logger.info('stopping _start....' + str(msvcrt.getch()))
                break


# I tryed it making it a static method
# but python did not let me :(
def _ascrActions(q, ascr_action, ascr_sleeep_time):

    while True:

        logger.info('_ascrActions is excecuting....')
        # We get all the postings in the queue q
        postings = []
        try:
            while True:
                postings.append(q.get(block=False))

        except Queue.Empty:
            pass
        if len(postings) > 0:
            ascr_action.do(postings)

        #seconds in a minute
        time.sleep(ascr_sleeep_time * 60)


if __name__ == '__main__':

    geoCoding = OSMGeocoding(city='Quebec', state='Quebec', country_code='CA',
                             user_email=USER_EMAIL)

    reference_point = {'lat': '46.801994', 'lon': '-71.233370'}

    rec = RadiousRestriction(geoCoding, reference_point, minimun_distance=30,
                             default=False)

    rss_url = 'http://www.kijiji.ca/rss-srp-appartement-condo-3-1-2/ville-de-quebec/c213l1700124?price=__900.00'

    source = KijijiRSS(rss_url, rec)
    #minutes
    waiting_time = -1
    action = SingleCSV("./", ['address', 'cost', 'link'])

    loop_up = LookUp(source, action, waiting_time)

    loop_up.run()
