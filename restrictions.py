

from geocoding import GeocodingAPI
from math import radians, cos, sin, asin, sqrt


class Restriction(object):

    def __init__(self):
        pass

    def is_valid(self, posting):
        raise NotImplementedError("Should have implemented this")


class AndRestriction(Restriction):

    def __init__(self, restrictions):
        if restrictions is None:
            self.restrictions = []
        else:
            self.restrictions = restrictions

    def add(self, restriction):
        self.append(restriction)

    def is_valid(self, posting):
        for restric in self.restrictions:
            if not restric.is_valid(posting):
                return False
        return True


class RadiousRestriction(Restriction):

    def __init__(self, geocoding_api, reference_point, minimun_distance,
                 default=False):

        if not isinstance(geocoding_api, GeocodingAPI):
            raise TypeError("data argument must be of type geocoding_api")

        self.geocoding_api = geocoding_api
        self.minimun_distance = minimun_distance
        self.default = default
        self.reference_lon = float(reference_point['lon'])
        self.reference_lat = float(reference_point['lat'])

    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    taken from:
    https://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points
    """
    def haversine(self, lon1, lat1, lon2, lat2):

        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))

        # 6367 km is the radius of the Earth
        km = 6367 * c
        return km

    def is_valid(self, posting):

        address = posting['address']

        geocoding_result = self.geocoding_api.search(address)
        # if we have a result
        if geocoding_result != []:
            posting_location = geocoding_result[0]

            # and that result has data
            if posting_location != []:
                posting_lon = float(posting_location['lon'])
                posting_lat = float(posting_location['lat'])

                aprox_distance = self.haversine(posting_lon, posting_lat,
                                                self.reference_lon,
                                                self.reference_lat)

                if aprox_distance <= self.minimun_distance:
                    return True
                return False

        return self.default
