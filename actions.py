

import csv
import time


class Action (object):

    def do(self, postings):
        raise NotImplementedError("Should have implemented this")


class MultipleAction (Action):

    def __init__(self, actions):

        self.actions = []
        if actions is not None and len(actions) > 0:
            self.actions = actions

    def appendAction(self, action):
        self.actions.append(action)

    def do(self, postings):

        for action in self.actions:
            action.do(postings)


class SingleCSV(Action):

    def __init__(self, path, header):
        self.path = path
        self.header = header

    def do(self, postings):

        sufix = str(time.time() * 10)
        file_path = self.path + '/postings_' + sufix + '.csv'

        with open(file_path, 'w') as output_file:

            listWriter = csv.DictWriter(output_file, fieldnames=self.header,
                                        delimiter=',',  quotechar='"',
                                        quoting=csv.QUOTE_MINIMAL,
                                         extrasaction='ignore')
            listWriter.writeheader()
            listWriter.writerows(postings)


class QuequeAction(Action):

    def __init__(self, queue):
        self.queue = queue

    def do(self, postings):
        self.queue.pu(postings)
