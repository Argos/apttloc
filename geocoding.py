
import urllib
import urllib2
import json

import logging
import logging.config

logging.config.fileConfig('logging.conf')

# create logger
logger = logging.getLogger('OSMGeocoding')


class GeocodingAPI(object):

    def search(self, query):
        raise NotImplementedError("Should have implemented this")


class OSMGeocoding(GeocodingAPI):

    def __init__(self, city=None, state=None,
                 country_code=None, user_email=None):
        self.city = city
        self.state = state
        self.country_code = country_code
        self.user_email = user_email
        self.service_loc = 'http://nominatim.openstreetmap.org/search'

    def search(self, query):

        '''
        When we have commas as for example in
        850 Avenue du Cardinal Begin, Quebec, QC G1S 3J1, Canada
        OSM does not find the address, as the information about the
        city and country is allready in the request
        for this reason we trim the address
        '''

        comma_loc = query.find(",")

        if comma_loc != -1:
            query = query[0:comma_loc]

        params = {"q": query,
                  'format': 'json',
                  'addressdetails': '1'}

        if self.city is not None:
            params['city'] = self.city
        if self.state is not None:
            params['state'] = self.state
        if self.country_code is not None:
            params['countrycodes'] = self.country_code
        if self.user_email is not None:
            params['email'] = self.user_email

        servi_url = self.service_loc + "?" + urllib.urlencode(params)
        result = urllib2.urlopen(servi_url, " ").read()

        if result == "[]":
            #Reporting in a log the invalid addresses
            
            logger.info(query)

        return json.loads(result)
