
import unittest
from mock import Mock, patch
from lxml import etree

import urllib2
import os

from geocoding import OSMGeocoding
from restrictions import RadiousRestriction
from sources import KijijiRSS
from actions import SingleCSV, MultipleAction
from lookup import LookUp


class Test(unittest.TestCase):

    def setUp(self):
        self.listing_feed = "./mock_calls/feed.rss"
        self.posting = "./mock_calls/posting.html"

        self.posting_tree = None

        with open(self.posting, "r") as posting_stream:
            parser = etree.HTMLParser()
            self.posting_tree = etree.parse(posting_stream, parser)

        self.geocoding_result = ''
        self.rentals = None

        with open("./mock_calls/geocoding.json", "r") as geo:
            self.geocoding_result = geo.read()
        with open("./mock_calls/rentals.txt", "r") as rentals_txt:
            #a bit of a hack :P
            self.rentals = eval(rentals_txt.read())

    def tearDown(self):

        for name in os.listdir('./mock_calls'):
            if name.find(".csv") != -1:
                print name
                os.remove('./mock_calls/' + name)

    def test_geocoding(self):

        with patch.object(urllib2, 'urlopen') as mock_request:
            mock_request.return_value = mock_response = Mock()
            attrs = {'read.return_value': self.geocoding_result}
            mock_response.configure_mock(**attrs)

            geo = OSMGeocoding(city="Quebec", state='Quebec', country_code='ca',
                               user_email='test@test.com')
            res = geo.search('630 rue st-test')[0]

            assert res['lon'] is not None
            assert res['lat'] is not None

    def test_distance_rest(self):

        with patch.object(OSMGeocoding, 'search') as mock_geo:

            mock_geo.return_value = [{"lat": "46.775140068956",
                                      "lon": "-71.3305147403296"}]
            reference_point = {"lat": "46.764939", "lon": "-71.313803"}
            rental = {}

            rental['address'] = '553 Rue Saint-Vallier Ouest'
            geo = OSMGeocoding(city="Quebec", state='Quebec',
                               country_code='ca', user_email='test@test.com')

            rest = RadiousRestriction(geo, reference_point, 30)

            assert rest.is_valid(rental)
            reference_point = {"lat": "46.354009", "lon": "-72.551553"}
            rest = RadiousRestriction(geo, reference_point, 30)
            assert not rest.is_valid(rental)

    def test_kijiji_feed(self):
        with patch.object(urllib2, 'urlopen') as mock_request, \
                patch.object(etree, 'parse') as mock_tree:

            mock_request.return_value = mock_response = Mock()

            posting_url = \
            'www.kijiji.ca/v-appartement-condo-studio-2-1-2/ville-de-quebec/quebec-plaines-21-2-semi-meuble/569070957'

            attrs = {'geturl.return_value': posting_url}
            mock_response.configure_mock(**attrs)

            mock_tree.return_value = self.posting_tree

            rss_url = \
            'http://www.kijiji.ca/rss-srp-appartement-condo-3-1-2/ville-de-quebec/c213l1700124?price=__900.00'

            sour = KijijiRSS(rss_url)
            rentals = sour.find()
            print rentals
            assert rentals is not None
            assert len(rentals) > 0

    def test_actionCSV(self):

        init_csv = len([name for name in os.listdir('./mock_calls')
                        if name.find(".csv") != -1])

        headers = ['address', 'cost', 'link']
        act = SingleCSV('./mock_calls', headers)
        act.do(self.rentals)

        final_csv = len([name for name in os.listdir('./mock_calls')
                         if name.find(".csv") != -1])

        assert init_csv + 1 == final_csv

    def test_lookup(self):
        mock_source = Mock()
        attrs = {'do.return_value': []}
        mock_source.configure_mock(**attrs)

        multActions = MultipleAction([])
        look = LookUp(mock_source, multActions, waiting_time=5. / 60,
                      ascr_action=multActions, ascr_sleeep_time=10. / 60)
        look.run()

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
